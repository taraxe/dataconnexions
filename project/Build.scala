import sbt._
import Keys._
import PlayProject._

object ApplicationBuild extends Build {

    val appName         = "data-connexion"
    val appVersion      = "1.0-SNAPSHOT"

    val appDependencies = Seq(
       "org.apache.poi" % "poi" % "3.8",
       "org.apache.poi" % "poi-scratchpad" % "3.8",
       "org.apache.poi" % "poi-ooxml" % "3.8"
    )

    val main = PlayProject(appName, appVersion, appDependencies, mainLang = SCALA).settings(
      // Add your own project settings here      
    )

}
