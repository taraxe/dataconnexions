import models._
import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import util._

/**
 * User: alabbe
 * Date: 18/06/12
 * Time: 18:45
 */

class InputData extends Specification {

   "Parsing csv " should {
      "succeed" in {
         running(FakeApplication()) {
            val r = CSVData("test.csv")
            r.size must equalTo(2)
         }
      }
   }
   "Parsing xlsx " should {
      "succeed " in {
         running(FakeApplication()) {
            val r = XLSData("test.xls")
            r.size must equalTo(90)
            r(0)(0) must equalTo("VOLUME MY PRACTICES")
         }
      }
   }

   "Reducing budget.csv" should {
      "succeed" in {
         running(FakeApplication()) {
            val rawWithHeader = CSVData("budget.csv")

            val raw = rawWithHeader.slice(4,rawWithHeader.size)
            val r = Spending.rawToStructured(raw)
            //println(r)
            //Gestion des finances publiques et des ressources humaines	302	5	Fiscalité douanière, énergétique et environnemental	414005947
            r.values.toList(0).values.toList(3).values.toList(0) mustEqual(214866282l)
         }
      }
   }

   "Reading from program programmes.csv" should {
      "succed" in {
         running(FakeApplication()){
            val raw = CSVData("programmes.csv").tail
            val reference = raw.map(l => (l(1).toInt -> l(2))).toMap
            reference
            reference(101) mustEqual("Accès au droit et à la justice")
         }
      }
   }

   "Constructing a Mission" should {
      "succeed with budget.csv and programmes.csv" in {
         running(FakeApplication()){
            val rawRef = CSVData("programmes.csv").tail
            val reference = rawRef.map(l => (l(1).toInt -> l(2))).toMap

            val rawWithHeader = CSVData("budget.csv")
            val raw = rawWithHeader.slice(4,rawWithHeader.size)

            val b = Spending("2010",Spending.rawToStructured(raw),reference)
            b.total mustEqual(378440172114l)
         }
      }
   }
}
