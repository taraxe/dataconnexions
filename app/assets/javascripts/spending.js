(function (ns, window) {

    var Spending = function () {
        $.ajax({
            url:jsRoutes.controllers.Application.data().url,
            type:"GET",
            dataType:"json"
        })
            .done(function (data) {
                console.log(data);
                var chart = new BubblePack();
                var selection = d3.select('#chart');
                selection.datum(data);
                chart.render(selection);

                $('tbody').on('click','tr', function (e) {

                    $(this).closest('tbody').find('tr').removeClass('active');
                    $(this).toggleClass('active');
                    var id = $(this).data('mid');
                    var clicked = d3.select('#' + id);
                    chart.zoom(clicked.data()[0]);

                });
            })
            .fail(function (xhr, status) {
                console.log(status)
            })
    }

    var BubblePack = function (config) {
        this.margin = { top:20, right:20, bottom:20, left:20};
        this.w = 940;
        this.h = 700;
        this.r = 700;
        this.x = d3.scale.linear().range([0, this.r]);
        this.y = d3.scale.linear().range([0, this.r]);
    }

    BubblePack.prototype.render = function (selection) {
        var self = this;

        selection.each(function (data) {
            var pack = d3.layout.pack()
                .size([self.r, self.r])
                .value(function (d) {
                    return d.cost;
                });

            self.vis = d3.select(this).selectAll("svg").data([data])
                .enter().append("svg")
                .attr("width", self.w)
                .attr("height", self.h)
                .append("svg:g")
                .attr("transform", "translate(" + (self.w - self.r) / 2 + "," + (self.h - self.r) / 2 + ")");

            self.node = data;
            self.root = data;

            var nodes = pack.nodes(self.root);

            var group = self.vis.selectAll("circle")
                .data(nodes)
                .enter()
                .append("svg:g");

            group.append("svg:circle")
                /*.dataum(function(d){
                    console.log(d);
                    return d;
                })*/
                .attr("class", function (d) {
                    return d.children ? "parent" : "child";
                })
                .attr("id", function (d) {
                    return d.id;
                })
                .attr("cx", function (d) {
                    return d.x;
                })
                .attr("cy", function (d) {
                    return d.y;
                })
                .attr("r", function (d) {
                    return d.r;
                })
                .on("click", function (d) {
                    return self.zoom(self.node == d ? self.root : d);
                });

            group.append("svg:text")
                .attr("class", function (d) {
                    return d.children ? "parent" : "child";
                })
                .attr("x", function (d) {
                    return d.x;
                })
                .attr("y", function (d) {
                    return d.y;
                })
                .attr("dy", ".35em")
                .attr("text-anchor", "middle")
                .style("opacity", function (d) {
                    return d.r > 50 ? 1 : 0;
                })
                .text(function (d) {
                    return d.name;
                })

            d3.select("#chart").on("click", function () {
                self.zoom(self.root);
            });
        })
    }

    BubblePack.prototype.zoom = function (d, i) {
        var self = this;

        var k = self.r / d.r / 2;
        self.x.domain([d.x - d.r, d.x + d.r]);
        self.y.domain([d.y - d.r, d.y + d.r]);

        var t = self.vis.transition()
            .duration(750);

        t.selectAll("circle")
            .attr("cx", function (d) {
                return self.x(d.x);
            })
            .attr("cy", function (d) {
                return self.y(d.y);
            })
            .attr("r", function (d) {
                return k * d.r;
            });

        t.selectAll("text")
            .attr("x", function (d) {
                return self.x(d.x);
            })
            .attr("y", function (d) {
                return self.y(d.y);
            })
            .style("opacity", function (d) {
                return k * d.r > 40 ? 1 : 0;
            });

        self.node = d;
        d3.event ? d3.event.stopPropagation() : {};
    }

    $(function () {
        ns.current = Spending();
        $(".table").tablesorter({
            onRenderHeader: function(index) {
                this.wrapInner('<span class="icons"></span>');
            }

        });
    })

})(window.spending = window.spending || {}, window);