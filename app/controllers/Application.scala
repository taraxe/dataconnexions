package controllers

import play.api._
import libs.json.Json
import play.api.mvc._
import util.CSVData
import models.Spending

object Application extends Controller {
   private implicit val app = play.api.Play.current
   private val rawWithHeader = CSVData("budget.csv")
   private val rawSpending = rawWithHeader.slice(4,rawWithHeader.size)

   val rawRef = CSVData("programmes.csv").tail
   val ref = rawRef.map(l => (l(1).toInt -> l(2))).toMap

   lazy val spending = Spending(data = Spending.rawToStructured(rawSpending), reference = ref)

   def index = Action { implicit request =>
      Ok(views.html.index("Budget 2011 France", spending))
   }
   def about = Action { implicit request =>
      Ok(views.html.about("A propos"))
   }

   /*REST API*/
   def data = Action{
      Ok(Json.toJson(spending))
   }

   def javascriptRoutes = Action { implicit request =>
      import routes.javascript._
      Ok(Routes.javascriptRouter("jsRoutes")(controllers.routes.javascript.Application.data))
   }
}