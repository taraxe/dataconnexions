package models

import play.api.libs.json._
import play.api.libs.json.JsString
import play.api.libs.json.JsObject
import play.api.libs.json.JsNumber
import org.apache.commons.lang.RandomStringUtils

/**
 * User: alabbe
 * Date: 20/06/12
 * Time: 22:18
 */
case class Spending(name:String, missions:Set[Mission]) {
   val total = missions.foldLeft(0l)(_+_.total)
}
case class Mission(name:String, programs:Set[Program], id:String=RandomStringUtils.random(10,true,false)){
   val total = programs.foldLeft(0l)(_+_.total)
}
case class Program(name:String, actions:Set[Action]) {
   val total = actions.foldLeft(0l)(_+_.cost)
}
case class Action(name:String, cost:Long)


object Spending {
   def rawToStructured(raw:List[List[String]]):Map[String,Map[String,Map[String,Long]]] = {
      raw.groupBy(_(0)).map{ mm =>
         (mm._1,mm._2.groupBy(_(1)).map{ pm =>
            (pm._1, pm._2.groupBy(_(2)).map{ am =>
               (am._2.foldLeft("")((r,i)=> r + i(3).trim),
                     am._2.foldLeft(0l)((r,i) => {r +(i(8).replace(" ","").toLong)}))
            })
         })
      }
   }
   def apply(name:String = "", data:Map[String,Map[String,Map[String,Long]]], reference:Map[Int,String]):Spending = {
      Spending(name,data.map(m =>
         Mission(m._1, m._2.map(p =>
            Program(reference.get(p._1.toInt).getOrElse(""),p._2.map( a=>
               Action(a._1, a._2)).toSet)
         ).toSet)
      ).toSet)
   }

   implicit object SpendingWriter extends Writes[Spending] {
      def writes(o: Spending) = JsObject(Seq(
         "name" -> JsString(o.name),
         "total" -> JsNumber(o.total),
         "children" -> JsArray(
            o.missions.map {
               m =>
                  JsObject(Seq(
                     "id" -> JsString(m.id),
                     "name" -> JsString(m.name),
                     "total" -> JsNumber(m.total),
                     "children" -> JsArray(
                        m.programs.map {
                           p =>
                              JsObject(Seq(
                                 "name" -> JsString(p.name),
                                 "total" -> JsNumber(p.total),
                                 "children" -> JsArray(
                                    p.actions.map {
                                       a =>
                                          JsObject(Seq(
                                             "name" -> JsString(a.name),
                                             "cost" -> JsNumber(a.cost)
                                          ))
                                    }.toSeq
                                 )
                              ))
                        }.toSeq
                     )
                  ))
            }.toSeq
         )
      ))
   }

}
