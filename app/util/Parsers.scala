package util

import play.api._
import java.io.File
import java.io.FileReader
import scala.util.parsing.combinator._
import org.apache.poi.ss.usermodel._


object CSVData extends RegexParsers {
   override def skipWhitespace = false

   val field = """([^;\n])*""".r
   val entry = repsep(field, ";") <~ "\n"
   val entries = rep(entry)

   import Play.current

   def apply(filename: String): List[List[String]] =
      parseAll(entries, new FileReader(new File(Play.getFile("data"), filename))) match {
         case Success(result, _) => result
         case f: Failure => throw new RuntimeException(f.toString)
      }
}

object XLSData {

   import Play.current
   import scala.collection.JavaConversions._

   implicit def iteratorToWrapper[T](iter: java.util.Iterator[T]): IteratorWrapper[T] = new IteratorWrapper[T](iter)

   def apply(filename: String): List[List[String]] = {
      require(filename.endsWith(".xls") || filename.endsWith(".xlxs"), "must be xls or xlsx file")
      val wb = WorkbookFactory.create(new File(Play.getFile("data"), filename))
      val result = collection.mutable.ListBuffer[List[String]]()

      for (i <- 0 until wb.getNumberOfSheets) {
         val sheet = wb.getSheetAt(i)

         for (row <- sheet.rowIterator) {
            val r = collection.mutable.ListBuffer[String]()
            for (cell <- row.cellIterator) {

               val value = cell.getCellType match {
                  case Cell.CELL_TYPE_STRING => cell.getRichStringCellValue.getString
                  case Cell.CELL_TYPE_NUMERIC => {
                     if (DateUtil.isCellDateFormatted(cell)) {
                        cell.getDateCellValue.toString
                     } else {
                        cell.getNumericCellValue.toString
                     }
                  }
                  case Cell.CELL_TYPE_BOOLEAN => cell.getBooleanCellValue.toString
                  case Cell.CELL_TYPE_FORMULA => cell.getCellFormula
                  case Cell.CELL_TYPE_BLANK => ""
               }
               r += value
            }
            result += r.toList
         }
      }
      result.toList
   }
}

