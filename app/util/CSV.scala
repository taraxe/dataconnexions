package util
import java.util.Date
import java.text.SimpleDateFormat

case class CSV(data: Seq[Seq[Any]]) {
  override def toString = data.map(CSV.printLine).mkString("", "\n", "\n")
}

object CSV {
  val isoDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")

  def apply(header: Seq[String], data: Seq[Map[String, Any]]): CSV = {
    CSV(header :: lines(header, data).toList)
  }

  def map(data: Seq[Map[String, Any]]): CSV =
    CSV(header(data), data)

  def header(data: Seq[Map[String, Any]]): List[String] =
    data.head.keys.toList

  def lines(header: Seq[String], data: Seq[Map[String, Any]]): Seq[Seq[Any]] =
    data.map(x => header.map(x))

  def printLine(data: Seq[Any]): String = {
    data.map(printCell).mkString(";")
  }

  def printCell[T](data: T): String = data match {
    case x: String => "\""+ x.replace("\"", "\"\"") +"\""
    case x: Number => x.toString
    case x: Date => isoDateFormat.format(x)
    case Some(x) => printCell(x)
    case None => ""
    case x => x.toString
  }
}
